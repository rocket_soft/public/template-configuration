package com.rocketsoft.module;

import com.rocketsoft.core.app.RocketApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = RocketApplication.class)
public class App {
    // DOCUMENTATION_BOOKMARK:1 https://rocket_soft_public.gitlab.io/documentation/
    public static void main(String[] args) {
        RocketApplication.run(App.class, args);
    }
}
