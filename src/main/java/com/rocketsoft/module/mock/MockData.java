package com.rocketsoft.module.mock;

import com.rocketsoft.core.app.init.InitBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Profile("!prod")
public class MockData implements InitBean {

    @Transactional
    public void init() {
        // Write init code here
    }
}
